package com.casumo.reviewtask;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Objects;
import java.util.Optional;

/**
 * This class is thread safe.
 */
public final class ParserFacade {

    private static ParserFacade instance;

    public static synchronized ParserFacade getInstance() {
        if (instance == null) {
            instance = new ParserFacade();
        }
        return instance;
    }

    private ParserFacade() {
    }

    private File file;

    public synchronized void setFile(File f) {
        file = f;
    }

    public synchronized Optional<File> getFile() {
        return Optional.ofNullable(file);
    }

    public String getContent() throws IOException {
        assertFileIsSet();
        return Files.readString(file.toPath());
    }

    private void assertFileIsSet() {
        Objects.requireNonNull(file, "File is not set!");
    }

    public String getContentWithoutUnicode() throws IOException {
        return getContent().replaceAll("\\P{ASCII}", "");
    }

    public void saveContent(String content) throws IOException {
        assertFileIsSet();
        Files.writeString(file.toPath(), content);
    }
}