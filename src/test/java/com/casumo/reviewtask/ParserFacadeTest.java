package com.casumo.reviewtask;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class ParserFacadeTest {

    private static final String FILE_WITH_UNICODE = "src/test/resources/unicode-content.txt";
    private static final String FILE_WITH_ASCII = "src/test/resources/ascii-content.txt";
    private static final String UNICODE_CONTENT = "Łukasz Dziedziul";
    private static final String ASCII_CONTENT = "Lukasz Dziedziul";

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @After
    public void tearDown() throws Exception {
        ParserFacade tested = ParserFacade.getInstance();
        tested.setFile(null);
    }

    @Test
    public void shouldReturnTheSameObject() throws Exception {
        //given
        ParserFacade tested1 = ParserFacade.getInstance();
        ParserFacade tested2 = ParserFacade.getInstance();
        //then
        assertThat(tested1, sameInstance(tested2));
    }

    @Test
    public void shouldGetContentFromAsciiFile() throws Exception {
        //given
        ParserFacade tested = ParserFacade.getInstance();
        //when
        tested.setFile(new File(FILE_WITH_ASCII));
        //then
        assertThat(tested.getContent(), is(ASCII_CONTENT));
    }

    @Test
    public void shouldGetContentFromUnicodeFile() throws Exception {
        //given
        ParserFacade tested = ParserFacade.getInstance();
        //when
        tested.setFile(new File(FILE_WITH_UNICODE));
        //then
        assertThat(tested.getContent(), is(UNICODE_CONTENT));
    }

    @Test
    public void shouldGetContentWithoutUnicodeFromAsciiFile() throws Exception {
        //given
        ParserFacade tested = ParserFacade.getInstance();
        //when
        tested.setFile(new File(FILE_WITH_ASCII));
        //then
        assertThat(tested.getContentWithoutUnicode(), is(ASCII_CONTENT));
    }

    @Test
    public void shouldGetContentWithoutUnicodeFromUnicodeFile() throws Exception {
        //given
        ParserFacade tested = ParserFacade.getInstance();
        //when
        tested.setFile(new File(FILE_WITH_UNICODE));
        //then
        assertThat(tested.getContentWithoutUnicode(), is("ukasz Dziedziul"));
    }

    @Test //run multiple times to see multiple instances. Flaky test by its nature
    public void shouldReturnSingletonForMultipleThreads() throws Exception {
        //given
        ExecutorService service = Executors.newFixedThreadPool(10);
        //when
        List<Future<ParserFacade>> futureList = IntStream.range(0, 100)
                .mapToObj(i -> service.submit(ParserFacade::getInstance))
                .collect(Collectors.toList());
        service.awaitTermination(2, TimeUnit.SECONDS);
        //then
        Set<ParserFacade> parserFacades = futureList.stream().map(this::getFutureResult).collect(Collectors.toSet());
        assertThat(parserFacades.size(), is(1));

    }

    private ParserFacade getFutureResult(Future<ParserFacade> parserFacadeFuture) {
        try {
            return parserFacadeFuture.get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void shouldSaveUnicodeContentToNewFile() throws Exception {
        //given
        ParserFacade tested = ParserFacade.getInstance();
        File file = new File(temporaryFolder.getRoot(), "nonexisting-file.txt");
        assertFalse(file.exists());
        tested.setFile(file);
        //when
        tested.saveContent(UNICODE_CONTENT);
        //then
        assertThat(tested.getContent(), is(UNICODE_CONTENT));
    }

    @Test
    public void shouldOverwriteExistingFile() throws Exception {
        //given
        ParserFacade tested = ParserFacade.getInstance();
        File file = temporaryFolder.newFile();
        assertTrue(file.exists());
        tested.setFile(file);
        //when
        tested.saveContent(UNICODE_CONTENT);
        //then
        assertThat(tested.getContent(), is(UNICODE_CONTENT));
    }

    @Test
    public void shouldFailSaveUnicodeContentWithoutFile() throws Exception {
        //given
        ParserFacade tested = ParserFacade.getInstance();
        tested.setFile(null);
        //then
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("File is not set!");
        //when
        tested.saveContent(UNICODE_CONTENT);
    }

    @Test
    public void shouldSaveAsciiContent() throws Exception {
        //given
        ParserFacade tested = ParserFacade.getInstance();
        tested.setFile(temporaryFolder.newFile());
        //when
        tested.saveContent(ASCII_CONTENT);
        //then
        assertThat(tested.getContent(), is(ASCII_CONTENT));
    }

    @Test
    public void shouldReturnOptionalWithFile() throws Exception {
        //given
        ParserFacade tested = ParserFacade.getInstance();
        //when
        tested.setFile(new File(FILE_WITH_ASCII));
        //then
        assertNotNull(tested.getFile());
        assertTrue(tested.getFile().isPresent());
        assertThat(tested.getFile().get(), is(new File(FILE_WITH_ASCII)));
    }

    @Test
    public void shouldReturnEmptyOptionalWhenNoFile() throws Exception {
        //given
        ParserFacade tested = ParserFacade.getInstance();
        //when
        tested.setFile(null);
        //then
        assertNotNull(tested.getFile());
        assertFalse(tested.getFile().isPresent());
    }
}